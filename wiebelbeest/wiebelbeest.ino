#include <Servo.h>
Servo myServo;

const int eye1 = A0;
const int eye2 = A1;

float v = 0;
float pos = 90;

void setup() {
  myServo.attach(9);
  myServo.write(pos);
}

void loop() {
  int in1 = analogRead(eye1);
  int in2 = analogRead(eye2);
  v = constrain(v + ((in1 - in2) * 0.05), -5, 5);
  pos += v;
  if (pos > 160) {
    pos = 160;
    v = 0;
  }
  if (pos < 20) {
    pos = 20;
    v = 0;
  }
  myServo.write(pos);
  delay(20);
}
